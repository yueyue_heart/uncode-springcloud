package cn.uncode.springcloud.starter.bus.mq.rabbit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class MessageWithTime {
    private String id;
    private long time;
    private Object message;
}
